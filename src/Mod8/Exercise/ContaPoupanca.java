package Mod8.Exercise;

public class ContaPoupanca extends Conta{
    private double rendimento;

    public ContaPoupanca(String nomeCliente, int contaBanco, int agencia, String banco, double saldo,double rendimento) {
        super(nomeCliente, contaBanco, agencia, banco, saldo);
        this.rendimento = rendimento;
    }

    public double getSaldo(){
        return this.saldo * this.rendimento;
    }
}
