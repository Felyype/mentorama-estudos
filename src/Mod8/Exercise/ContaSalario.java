package Mod8.Exercise;

public class ContaSalario extends Conta {

    public ContaSalario(String nomeCliente, int contaBanco, int agencia, String banco, double saldo) {
        super(nomeCliente, contaBanco, agencia, banco, saldo);
    }

    public double getSaldo(){
        return this.saldo;
    }
}
