package Mod8.Exercise;

import java.util.Scanner;

public class Menus {

    public static void menuCliente(){
        System.out.println("******************");
        System.out.println("Bem vindo(a) ao " + ControlesBanco.NOME_DO_BANCO);
        System.out.println("******************\n");
        System.out.println("Como posso te ajudar!?");
        System.out.println("1 - Criar uma conta no " + ControlesBanco.NOME_DO_BANCO);
        System.out.println("2 - Ver hist�ria do " + ControlesBanco.NOME_DO_BANCO);
        System.out.println("3 - Sair");
    }

    public static void funcaoMenuCliente(){
        Scanner input = new Scanner(System.in);
        String escolhaMenuCliente = input.next();

        switch (escolhaMenuCliente){
            case "1": {
                System.out.println("Por favor preencher os campos abaixos de acordo com nosso atendente virtual :)\n");
                System.out.println("1 - Seu nome.");
                String nomeCliente = input.next();
                System.out.println("2 - O tipo da conta. ( Corrente, poupan�a ou sal�rio ) ");
                String tipoDaConta = input.next();

                if(tipoDaConta.endsWith("nte")) {
                    System.out.print("Conta Corrente");
                } else if(tipoDaConta.endsWith("n�a") || tipoDaConta.endsWith("nca")){
                    System.out.println("Conta Poupan�a");
                } else if(tipoDaConta.endsWith("rio")) {
                    System.out.println("Conta Sal�rio");
                } else {
                    System.out.println("Digite de forma correta ;)");
                }
                break;
            }

            case "2": {
                System.out.println("Vendo Historia...");
                break;
            }

            case "3": {
                System.out.println("Saindo..");
                break;
            }

            default: {
                System.out.println("Digite uma op��o v�lida.");
                break;
            }
        }
    }
}
