package Mod8.Exercise;

public abstract class Conta {
    private String nomeCliente;
    private int contaBanco;
    private int agencia;
    private String banco;
    protected double saldo;

    public String getNomeCliente() {
        return nomeCliente;
    }

    public int getContaBanco() {
        return contaBanco;
    }

    public int getAgencia() {
        return agencia;
    }

    public String getBanco() {
        return banco;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Conta(String nomeCliente, int contaBanco, int agencia, String banco, double saldo) {
        this.nomeCliente = nomeCliente;
        this.contaBanco = contaBanco;
        this.agencia = agencia;
        this.banco = banco;
        this.saldo = saldo;
    }

}
