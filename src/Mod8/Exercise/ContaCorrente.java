package Mod8.Exercise;

public class ContaCorrente extends Conta{
    private double chequeEspecial;

    public ContaCorrente(String nomeCliente, int contaBanco, int agencia, String banco, double saldo, double chequeEspecial) {
        super(nomeCliente, contaBanco, agencia, banco, saldo);
        this.chequeEspecial = chequeEspecial;
    }

    public double getSaldo(){
        return this.saldo + this.chequeEspecial;
    }

    public void setSaldo(double saldo, double chequeEspecial){
        this.saldo = saldo;
        this.chequeEspecial = chequeEspecial;
    }

}
