package Modulo8;

public class ContaSalario extends Conta {
	
	private int limiteDeSaque;
	
	public ContaSalario(int numero, int agencia, String banco, double saldo) {
		super(numero, agencia, banco, saldo);
	}
	
	public double getSaldo() {
		return saldo;
	}

	@Override
	public void saque(double valor) {
		if(this.saldo < valor || valor == 0 || valor < this.saldo) {
			return;
		}
		
		if(limiteDeSaque == 2) {
			System.out.println("Voc� bateu o limite de saques.");
			return;
		}
		
		this.saldo -= valor;
		limiteDeSaque++;
		System.out.println("Sacado!");
		
	}

	@Override
	public void deposito(double valor) {
		if(this.saldo < valor || valor == 0) {
			System.out.println("Voc� tentou depositar um valor inv�lido, por favor, tente novamente.");
			return;
		}
		
		this.saldo += valor;
		System.out.println("Depositado!");
		
	}
	
}
