package Modulo8;

public class ContaCorrente extends Conta implements Tributavel{
	
	private int limiteDeDepositos;
	private double chequeEspecial;

	public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
		super(numero, agencia, banco, saldo);
		this.chequeEspecial = chequeEspecial;
	}

	@Override
	public String toString() {
		return "ContaCorrente [chequeEspecial=" + chequeEspecial + "]";
	}
	
	public double getSaldo() {
		return this.chequeEspecial + this.saldo;
	}
	
	public void setSaldo(double valorCheque, double valorSaldo) {
		this.chequeEspecial = valorCheque;
		this.saldo = valorSaldo;
	}
	
	@Override
	public void saque(double valor) {
		if(valor > getSaldo()) {
			return;
		}
		this.saldo -= valor;
		System.out.println("Sacado!");
	}
	
	@Override
	public void deposito(double valor) {
		if(this.saldo < valor || valor == 0) {
			System.out.println("Voc� tentou depositar um valor inv��lido, por favor, tente novamente.");
			return;
		}
		this.saldo += valor;
		System.out.println("Depositado!");
	}

	@Override
	public double getValorImposto() {
		return this.getSaldo() * 0.01;
	}
}
