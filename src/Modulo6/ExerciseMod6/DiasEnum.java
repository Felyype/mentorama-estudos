package Modulo6.ExerciseMod6;

public enum DiasEnum{
	
	SEGUNDA("Segunda"), 
	TERCA("Ter�a"), 
	QUARTA("Quarta"), 
	QUINTA("Quinta"), 
	SEXTA("Sexta");
	
	private String diaSemana;
	
	DiasEnum(String diaSemana) {
		this.diaSemana = diaSemana;
	}
	
	public String getDiaSemana() {
		return diaSemana;
	}
}
