package Modulo7.Exercise;

public class ContaCorrente extends Conta {
	
	private int limiteDeDepositos;
	private double chequeEspecial;

	public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
		super(numero, agencia, banco, saldo);
		this.chequeEspecial = chequeEspecial;
	}

	@Override
	public String toString() {
		return "ContaCorrente [chequeEspecial=" + chequeEspecial + "]";
	}
	
	public double getSaldo() {
		return this.chequeEspecial + this.saldo;
	}
	
	public void setSaldo(double valorCheque, double valorSaldo) {
		this.chequeEspecial = valorCheque;
		this.saldo = valorSaldo;
	}
	
	@Override
	public void saque(double valor) {
		if(valor <= 0) {
			System.out.println("Voc� tentou sacar um n�mero inv�lido ( 0 ou menor que 0 ).");
			return;
		}
		if(valor > getSaldo()) {
			System.out.println("Voc� nao tem saldo o suficiente para este saque.");
			return;
		}
		this.saldo -= valor;
		System.out.println("Sacado!");
	}
	
	@Override
	public void deposito(double valor) {
		if(this.saldo < valor || valor == 0) {
			System.out.println("Voc� tentou depositar um valor inv��lido, por favor, tente novamente.");
			return;
		}
		this.saldo += valor;
		System.out.println("Depositado!");
	}
	
}
