package Modulo7.Exercise;

public abstract class Conta {
	private int numero;
	private int agencia;
	private String banco;
	protected double saldo;
	private double cashBack;
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public double getSaldo(){
		return this.saldo;
	};
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	//Saque, Dep�sito e Transfer�ncia.
	
	public abstract void saque(double valor);
	
	public abstract void deposito(double valor);
	
	/*public void transferencia(Conta contaAReceber, double valorParaTransferir) {
		if(this.getSaldo() <= 0 || valorParaTransferir > this.getSaldo() || valorParaTransferir == 0) {
			System.out.println("Voc� tentou enviar um valor inv�lido, por favor, tente novamente.");
			return;
		}
		
		if(valorParaTransferir >= 200.00) {
			cashBack = 0.1 * valorParaTransferir;
			this.setSaldo(this.getSaldo() + cashBack);
		}
		
		this.setSaldo(this.getSaldo() - valorParaTransferir);
		contaAReceber.setSaldo(contaAReceber.getSaldo() + valorParaTransferir);
		
		System.out.println("Dinheiro transferido com Sucesso!");
		System.out.println(cashBack > 0 ? "Voc� recebeu um Cash Back de: "+cashBack+ " por transferir um valor acima de R$2OO,00 :)" :
			"Sem Cash Back para voc�, pois n�o foi transferido nenhum valor acima de R$2OO,00 :(");
		
	}
	 */

	public Conta(int numero, int agencia, String banco, double saldo) {
		
		this.numero = numero;
		this.agencia = agencia;
		this.banco = banco;
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return "Conta [numero=" + numero + ", agencia=" + agencia + ", banco=" + banco + ", saldo=" + saldo + "]";
	}

}
