package Modulo7.Exercise;

public class ContaPoupanca extends Conta {
	private int diaAniversario;
	private double taxaDeJuros;
	private double gratificacaoAniversario;
	
	public ContaPoupanca(int numero, int agencia, String banco, double saldo
			,int diaAniversario, double taxaDeJuros, double gratificacaoAniversario) {
		super(numero, agencia, banco, saldo);
		this.diaAniversario = diaAniversario;
		this.taxaDeJuros = taxaDeJuros;
		this.gratificacaoAniversario = gratificacaoAniversario;
	}

	public double getSaldo(int diaAniversario) {
		if(this.diaAniversario == diaAniversario){
			return this.saldo *= (this.taxaDeJuros * this.gratificacaoAniversario);
		}
		return this.saldo *= this.taxaDeJuros;
	}

	@Override
	public void saque(double valor) {
		if(this.saldo < valor || valor == 0 || valor < this.saldo) {
			return;
		}
		this.saldo -= valor;
		System.out.println("Sacado!");
		
	}

	@Override
	public void deposito(double valor) {
		if(this.saldo < valor || valor == 0) {
			System.out.println("Voc� tentou depositar um valor inv�lido, por favor, tente novamente.");
			return;
		}
		
		this.saldo += valor;
		System.out.println("Depositado!");
		
	}
	
}
